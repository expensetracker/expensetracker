#ifndef EXPENSES_H
#define EXPENSES_H

#include "category.h"
#include "expense.h"

#include <QHash>
#include <QList>
#include <QObject>
#include <QSettings>

/**
  @brief Holds information about expenses

  The Expenses class holds information about actual expenses but also
  some meta information which is used to provide further values to expenses,
  group them, allows to make analyses and so forth.
  */
class Expenses : public QObject
{

  Q_OBJECT

public:

  explicit Expenses(QObject *parent = 0);
  virtual ~Expenses();

  inline int expenseCount() const;
  inline const Expense& expense( int index ) const;
  inline int categoryCount() const;
  inline const Category& categoryAt( int index ) const;
  inline Category category( int id ) const;

  void saveData( QSettings& settings ) const;
  void restoreData( QSettings& settings );

signals:

  void expenseAdded( int index );
  void expenseRemoved( int index );
  void expenseChanged( int index );
  void expensesReset();
  void categoriesChanged();

public slots:

  void addExpense( const Expense& expense );
  void removeExpense( int index );
  void replaceExpense( int index, const Expense& expense );
  void addCategory( const Category& category );
  void removeCategory( const Category& category );
  void replaceCategory( const Category& category );

private:

  QList< Expense >        m_expenses;
  QHash< int, Category >  m_categories;

};

int Expenses::expenseCount() const
{
  return m_expenses.count();
}

const Expense& Expenses::expense(int index) const
{
  return m_expenses.at( index );
}

int Expenses::categoryCount() const
{
  return m_categories.count();
}

const Category& Expenses::categoryAt(int index) const
{
  return m_categories.values().at( index );
}

Category Expenses::category( int id ) const
{
  return m_categories.value( id, Category() );
}

#endif // EXPENSES_H
