#include "expensessortfilterproxymodel.h"

ExpensesSortFilterProxyModel::ExpensesSortFilterProxyModel( ExpensesModel *model,
                                                            QObject *parent ) :
  QSortFilterProxyModel(parent),
  m_sourceModel( model )
{
  setSourceModel( model );
  setDynamicSortFilter( true );
}

const Expense& ExpensesSortFilterProxyModel::expense( const QModelIndex &index ) const
{
  return m_sourceModel->expense( mapToSource( index ) );
}

void ExpensesSortFilterProxyModel::removeExpense( const QModelIndexList &indices )
{
  QModelIndexList tmp;
  foreach ( QModelIndex index, indices )
  {
    tmp << mapToSource( index );
  }
  m_sourceModel->removeExpense( tmp );
}

void ExpensesSortFilterProxyModel::replaceExpense( const QModelIndex &index, Expense newExpense )
{
  m_sourceModel->replaceExpense( mapToSource( index ), newExpense );
}

bool ExpensesSortFilterProxyModel::lessThan( const QModelIndex &left,
                                             const QModelIndex &right ) const
{
  switch ( left.column() )
  {
  case ExpensesModel::DateColumn:
  {
    Expense leftExpense = m_sourceModel->expense( left );
    Expense rightExpense = m_sourceModel->expense( right );
    return leftExpense.date() < rightExpense.date();
  }

  case ExpensesModel::AmountColumn:
  {
    Expense leftExpense = m_sourceModel->expense( left );
    Expense rightExpense = m_sourceModel->expense( right );
    return leftExpense.amount() < rightExpense.amount();
  }

  default: return QSortFilterProxyModel::lessThan( left, right );
  }
}

void ExpensesSortFilterProxyModel::setSourceModel( QAbstractItemModel *sourceModel )
{
  QSortFilterProxyModel::setSourceModel( sourceModel );
}
