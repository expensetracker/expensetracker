#ifndef CALCULATORDIALOG_H
#define CALCULATORDIALOG_H

#include <QDialog>

namespace Ui {
    class CalculatorDialog;
}

class CalculatorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalculatorDialog(QWidget *parent = 0);
    ~CalculatorDialog();

    void setValue( double value );
    double value() const;

private:
    Ui::CalculatorDialog *ui;

    double calculateValue( bool& ok ) const;

private slots:

    void onTextChanged();
    void tryAccept();

};

#endif // CALCULATORDIALOG_H
