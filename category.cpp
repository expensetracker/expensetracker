#include "category.h"

Category::Category( int id, const QString& name ) :
  m_id( id ),
  m_name( name )
{
}

Category::~Category()
{
}

void Category::saveData( QSettings &settings ) const
{
  settings.setValue( "id", m_id );
  settings.setValue( "name", m_name );
}

void Category::restoreData( QSettings &settings )
{
  m_id = settings.value( "id", InvalidCategory ).toInt();
  m_name = settings.value( "name", QString() ).toString();
}
