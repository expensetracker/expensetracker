#ifndef CATEGORY_H
#define CATEGORY_H

#include <QSettings>
#include <QString>

/**
  @brief Meta information about a single category an expense can be tagged with

  The Category class holds information about a single category an Expense can
  be tagged with. A category itself is indeed just a numeric value.
  The category class provides further information to enrich that value
  (mainly for being shown to the user).
  */
class Category
{
public:

  /// Special Categories
  typedef enum
  {
    InvalidCategory = -1 //!< Special value used to indicate a category which is guaranteed to be "invalid"
  } SpecialCategories;

  Category( int id = InvalidCategory, const QString& name = QString() );
  virtual ~Category();

  inline int id() const;
  inline const QString& name() const;
  inline void setName( const QString& name );
  inline bool isValid() const;

  void saveData( QSettings& settings ) const;
  void restoreData( QSettings& settings );

private:

  int       m_id;
  QString   m_name;

};

int Category::id() const
{
  return m_id;
}

const QString& Category::name() const
{
  return m_name;
}

void Category::setName( const QString &name )
{
  m_name = name;
}

bool Category::isValid() const
{
  return m_id >= 0;
}

#endif // CATEGORY_H
