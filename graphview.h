#ifndef GRAPHVIEW_H
#define GRAPHVIEW_H

#include "expenses.h"

#include <QWidget>
#include <QWebView>

class GraphView : public QWidget
{

    Q_OBJECT

public:

    typedef struct ExpensesPerYearAndMonth {
        unsigned int         month;
        unsigned int         year;
        double               totalExpense;
        QHash< int, double > totalExpensePerCategory;

        ExpensesPerYearAndMonth() :
            month( 0 ),
            year( 0 ),
            totalExpense( 0.0 ),
            totalExpensePerCategory( QHash< int, double >() )
        {
        }
    } ExpensesPerYearAndMonth;

    explicit GraphView(Expenses* expenses, QWidget *parent = 0);

signals:

public slots:

    void redraw();

private:

    Expenses* m_expenses;
    QWebView* m_view;
    QString   m_pageTemplate;

    void updateGraph();
    QHash< quint64, ExpensesPerYearAndMonth > accumulateExpenses() const;
    QString accumulatedExpensesToCsvString(
            QHash< quint64, ExpensesPerYearAndMonth > accumulatedExpenses ) const;

};

#endif // GRAPHVIEW_H
