#ifndef CATEGORYCONFIGURATIONDIALOG_H
#define CATEGORYCONFIGURATIONDIALOG_H

#include "expenses.h"

#include <QDialog>

namespace Ui {
class CategoryConfigurationDialog;
}

class CategoryConfigurationDialog : public QDialog
{
  Q_OBJECT

public:
  explicit CategoryConfigurationDialog(Expenses* expenses, QWidget *parent = 0);
  ~CategoryConfigurationDialog();

private:
  Ui::CategoryConfigurationDialog*  ui;
  Expenses*                         m_expenses;

private slots:

  void addCategory();
  void renameCategory();
  void deleteCategory();

};

#endif // CATEGORYCONFIGURATIONDIALOG_H
