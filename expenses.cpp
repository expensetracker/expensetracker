#include "expenses.h"

Expenses::Expenses(QObject *parent) :
  QObject(parent),
  m_expenses( QList< Expense >() ),
  m_categories( QHash< int, Category >() )
{
}

Expenses::~Expenses()
{
}

void Expenses::saveData( QSettings &settings ) const
{
  settings.beginWriteArray( "expenses", m_expenses.count() );
  for ( int i = 0; i < m_expenses.count(); ++i )
  {
    settings.setArrayIndex( i );
    m_expenses[ i ].saveData( settings );
  }
  settings.endArray();

  settings.beginWriteArray( "categories", m_categories.count() );
  for ( int i = 0; i < m_categories.count(); ++i )
  {
    settings.setArrayIndex( i );
    m_categories.values()[ i ].saveData( settings );
  }
  settings.endArray();
}

void Expenses::restoreData( QSettings &settings )
{
  m_expenses.clear();
  int numExpenses = settings.beginReadArray( "expenses" );
  for ( int i = 0; i < numExpenses; ++i )
  {
    settings.setArrayIndex( i );
    Expense expense;
    expense.restoreData( settings );
    m_expenses.append( expense );
  }
  settings.endArray();

  m_categories.clear();
  int numCategories = settings.beginReadArray( "categories" );
  for ( int i = 0; i < numCategories; ++i )
  {
    settings.setArrayIndex( i );
    Category category;
    category.restoreData( settings );
    if ( category.isValid() )
    {
      m_categories.insert( category.id(), category );
    }
  }
  settings.endArray();

  emit categoriesChanged();
  emit expensesReset();
}

void Expenses::addExpense( const Expense &expense )
{
  m_expenses.append( expense );
  emit expenseAdded( m_expenses.count() - 1 );
}

void Expenses::removeExpense( int index )
{
  m_expenses.removeAt( index );
  emit expenseRemoved( index );
}

void Expenses::replaceExpense( int index, const Expense &expense )
{
  m_expenses[ index ] = expense;
  emit expenseChanged( index );
}

void Expenses::addCategory( const Category &category )
{
  Q_ASSERT( ! m_categories.contains( category.id() ) );
  Q_ASSERT( category.isValid() );
  m_categories.insert( category.id(), category );
  emit categoriesChanged();
}

void Expenses::removeCategory( const Category &category )
{
  Q_ASSERT( m_categories.contains( category.id() ) );
  for ( int i = 0; i < m_expenses.count(); ++i )
  {
    m_expenses[ i ].removeCategory( category.id() );
  }
  m_categories.remove( category.id() );
  emit categoriesChanged();
}

void Expenses::replaceCategory( const Category &category )
{
  Q_ASSERT( m_categories.contains( category.id() ) );
  m_categories.insert( category.id(), category );
  emit categoriesChanged();
}

