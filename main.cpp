#include <QtGui/QApplication>
#include "mainwindow.h"

#ifndef VERSION
#define VERSION "unknown";
#endif

int main(int argc, char *argv[])
{
  QCoreApplication::setApplicationName( "ExpenseTracker" );
  QCoreApplication::setOrganizationName( "RPdev" );
  QCoreApplication::setOrganizationDomain( "www.rpdev.net" );

  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  return a.exec();
}
