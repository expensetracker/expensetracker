#ifndef EXPENSEEDITDIALOG_H
#define EXPENSEEDITDIALOG_H

#include "expenses.h"

#include <QDialog>

namespace Ui {
class ExpenseEditDialog;
}

class ExpenseEditDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ExpenseEditDialog( Expenses* expenses, QWidget *parent = 0 );
  ~ExpenseEditDialog();

  Expense expense() const;
  void setExpense( const Expense& expense );

private:
  Ui::ExpenseEditDialog*  ui;
  Expenses*               m_expenses;
  QList< int >            m_categories;

  void updateCategoryControl();

private slots:
  void editCategories();
  void editAmount();

};

#endif // EXPENSEEDITDIALOG_H
