#ifndef EXPENSE_H
#define EXPENSE_H

#include <QDate>
#include <QList>
#include <QSettings>
#include <QString>

/**
  @brief A single expense

  Holds information about a single expense. An expense it basically an amount
  of money spent, together with some "meta" information:
  <ul>
    <li>A descriptive text to indicate what the money has been spent for</li>
    <li>The date the money has been spent</li>
    <li>A list of categories (used for filtering)</li>
  </ul>
  */
class Expense
{
public:

  Expense( double amount = 0.0,
           const QString& description = QString(),
           const QDate& date = QDate(),
           const QList< int >& categories = QList< int >() );
  virtual ~Expense();

  inline double amount() const;
  inline void setAmount( double amount );
  inline const QString& description() const;
  inline void setDescription( const QString& description );
  inline const QList< int >& categories() const;
  inline void setCategories( const QList< int >& categories );
  inline void addCategory( int category );
  inline void removeCategory( int category );
  inline QDate date() const;
  inline void setDate( QDate date );

  void saveData( QSettings& settings ) const;
  void restoreData( QSettings& settings );

private:

  double        m_amount;
  QString       m_description;
  QList< int >  m_categories;
  QDate         m_date;

};

double Expense::amount() const
{
  return m_amount;
}

void Expense::setAmount( double amount )
{
  m_amount = amount;
}

const QString& Expense::description() const
{
  return m_description;
}

void Expense::setDescription( const QString &description )
{
  m_description = description;
}

const QList< int >& Expense::categories() const
{
  return m_categories;
}

void Expense::setCategories( const QList<int> &categories )
{
  m_categories.clear();
  foreach ( int category, categories )
  {
    addCategory( category );
  }
}

void Expense::addCategory( int category )
{
  if ( ! m_categories.contains( category ) )
  {
    m_categories.append( category );
  }
}

void Expense::removeCategory( int category )
{
  m_categories.removeAll( category );
}

QDate Expense::date() const
{
  return m_date;
}

void Expense::setDate( QDate date )
{
  m_date = date;
}

#endif // EXPENSE_H
