#-------------------------------------------------
#
# Project created by QtCreator 2011-09-10T21:09:46
#
#-------------------------------------------------

QT       += core gui webkit

TARGET = ExpenseTracker
TEMPLATE = app

VERSION = 0.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    expense.cpp \
    category.cpp \
    expenses.cpp \
    expensesmodel.cpp \
    expenseeditdialog.cpp \
    categoryconfigurationdialog.cpp \
    categoryselectiondialog.cpp \
    expensessortfilterproxymodel.cpp \
    calculatordialog.cpp \
    graphview.cpp

HEADERS  += mainwindow.h \
    expense.h \
    category.h \
    expenses.h \
    expensesmodel.h \
    expenseeditdialog.h \
    categoryconfigurationdialog.h \
    categoryselectiondialog.h \
    expensessortfilterproxymodel.h \
    calculatordialog.h \
    graphview.h

FORMS    += mainwindow.ui \
    expenseeditdialog.ui \
    categoryconfigurationdialog.ui \
    categoryselectiondialog.ui \
    calculatordialog.ui

RESOURCES += \
    resources.qrc

OTHER_FILES += \
    graphviewtemplate.html







