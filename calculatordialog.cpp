#include "calculatordialog.h"
#include "ui_calculatordialog.h"

CalculatorDialog::CalculatorDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalculatorDialog)
{
    ui->setupUi(this);
    connect( ui->lines, SIGNAL(textChanged()),
             this, SLOT(onTextChanged()) );
    connect( ui->buttonBox, SIGNAL(accepted()),
             this, SLOT(tryAccept()) );
    ui->lines->setFocus();
}

CalculatorDialog::~CalculatorDialog()
{
    delete ui;
}

void CalculatorDialog::setValue( double value )
{
    if ( value == 0. )
    {
        ui->lines->clear();
    } else
    {
        ui->lines->setPlainText( QString::number( value ) );
    }
    ui->lines->setAlignment( Qt::AlignRight );
}

double CalculatorDialog::value() const
{
    bool ok;
    return calculateValue( ok );
}

double CalculatorDialog::calculateValue(bool &ok) const
{
    double result = 0.;
    ok = true;
    QStringList entries = ui->lines->toPlainText().split(
                QRegExp( "\n" ), QString::SkipEmptyParts );
    foreach ( QString entry, entries )
    {
        bool entryOk;
        double val = entry.toDouble( &entryOk );
        if ( entryOk )
        {
            result += val;
        } else
        {
            ok = false;
            return 0.;
        }
    }
    return result;
}

void CalculatorDialog::onTextChanged()
{
    bool ok;
    double value = calculateValue( ok );
    if ( ok )
    {
        ui->valueLabel->setText( QString::number( value ) );
    } else
    {
        ui->valueLabel->setText( tr( "Invalid" ) );
    }
}

void CalculatorDialog::tryAccept()
{
    bool ok;
    calculateValue( ok );
    if ( ok )
    {
        accept();
    }
}
