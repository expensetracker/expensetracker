#ifndef CATEGORYSELECTIONDIALOG_H
#define CATEGORYSELECTIONDIALOG_H

#include "expenses.h"

#include <QDialog>

namespace Ui {
class CategorySelectionDialog;
}

class CategorySelectionDialog : public QDialog
{
  Q_OBJECT

public:
  explicit CategorySelectionDialog(Expenses* expenses,
                                   const QList< int >& categories,
                                   QWidget *parent = 0);
  ~CategorySelectionDialog();

  QList< int > categories() const;

private:

  Ui::CategorySelectionDialog* ui;
};

#endif // CATEGORYSELECTIONDIALOG_H
