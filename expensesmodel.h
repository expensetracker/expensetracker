#ifndef EXPENSESMODEL_H
#define EXPENSESMODEL_H

#include "expenses.h"

#include <QAbstractItemModel>

class ExpensesModel : public QAbstractItemModel
{

  Q_OBJECT

public:

  typedef enum
  {
    AmountColumn = 0,
    DescriptionColumn,
    DateColumn,
    CategoriesColumn,
    ColumnCount
  } Columns;

  explicit ExpensesModel( Expenses* expenses, QObject *parent = 0 );
  virtual ~ExpensesModel();

  const Expense& expense( const QModelIndex& index ) const;
  void removeExpense( const QModelIndexList& indices );
  void replaceExpense( const QModelIndex& index, Expense newExpense );

  // QAbstractItemModel interface implementation

  virtual int rowCount( const QModelIndex &parent = QModelIndex() ) const;
  virtual int columnCount( const QModelIndex &parent = QModelIndex() ) const;
  virtual QModelIndex index( int row,
                             int column,
                             const QModelIndex &parent = QModelIndex() ) const;
  virtual QModelIndex parent( const QModelIndex &child ) const;
  virtual QVariant headerData( int section,
                               Qt::Orientation orientation, int role) const;
  virtual QVariant data( const QModelIndex &index, int role) const;


signals:

public slots:

private slots:

  void expenseAdded( int index );
  void expenseRemoved( int index );
  void expenseChanged( int index );
  void expensesReset();
  void categoriesChanged();

private:

  Expenses* m_expenses;

};

#endif // EXPENSESMODEL_H
