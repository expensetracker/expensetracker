#ifndef EXPENSESSORTFILTERPROXYMODEL_H
#define EXPENSESSORTFILTERPROXYMODEL_H

#include "expensesmodel.h"

#include <QSortFilterProxyModel>

class ExpensesSortFilterProxyModel : public QSortFilterProxyModel
{

  Q_OBJECT

public:

  explicit ExpensesSortFilterProxyModel( ExpensesModel* model, QObject *parent = 0);

  // Mirrored from ExpensesModel
  const Expense& expense( const QModelIndex& index ) const;
  void removeExpense( const QModelIndexList& indices );
  void replaceExpense( const QModelIndex& index, Expense newExpense );

signals:

public slots:

protected:

  bool lessThan( const QModelIndex &left, const QModelIndex &right ) const;

private:

  ExpensesModel*    m_sourceModel;

  virtual void setSourceModel( QAbstractItemModel *sourceModel );

};

#endif // EXPENSESSORTFILTERPROXYMODEL_H
