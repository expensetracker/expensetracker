#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "categoryconfigurationdialog.h"
#include "expenseeditdialog.h"
#include "graphview.h"

#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  m_expenses( new Expenses( this ) ),
  m_expensesModel( new ExpensesModel( m_expenses, this ) ),
  m_expensesSortFilterProxyModel( new ExpensesSortFilterProxyModel( m_expensesModel, this ) )
{
  ui->setupUi(this);
  ui->expenseView->setModel( m_expensesSortFilterProxyModel );

  m_graphView = new GraphView( m_expenses, ui->graphTab );
  ui->graphTab->setLayout( new QGridLayout( ui->graphTab ) );
  ui->graphTab->layout()->addWidget( m_graphView );

  connect( ui->exportIniAction, SIGNAL(triggered()), this, SLOT(exportIni()) );
  connect( ui->importIniAction, SIGNAL(triggered()), this, SLOT(importIni()) );
  connect( ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()) );
  connect( ui->actionAddExpense, SIGNAL(triggered()), this, SLOT(addExpense()) );
  connect( ui->actionEditExpense, SIGNAL(triggered()), this, SLOT(editExpense()) );
  connect( ui->actionRemoveExpense, SIGNAL(triggered()), this, SLOT(removeExpense()) );
  connect( ui->configureCategoriesAction, SIGNAL(triggered()), this, SLOT(configureCategories()) );
  connect( ui->expenseView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(itemDoubleClicked(QModelIndex)) );

  // Add some of the menu actions to main view's context menu
  ui->expenseView->addAction( ui->actionAddExpense );
  ui->expenseView->addAction( ui->actionEditExpense );
  ui->expenseView->addAction( ui->actionRemoveExpense );
  ui->expenseView->addAction( ui->configureCategoriesAction );

  QSettings settings;
  restoreData( settings );
}

MainWindow::~MainWindow()
{
  QSettings settings;
  saveData( settings );

  delete ui;
}

void MainWindow::saveData( QSettings& settings, DataFormat format ) const
{
  settings.beginGroup( "data" );
  m_expenses->saveData( settings );
  settings.endGroup();
  if ( format == FullData )
  {
    settings.beginGroup( "layout" );
    settings.setValue( "geometry", saveGeometry() );
    settings.setValue( "state", saveState() );
    settings.setValue( "expenseViewColumnsGeometry", ui->expenseView->header()->saveGeometry() );
    settings.setValue( "expenseViewColumnsState", ui->expenseView->header()->saveState() );
    settings.endGroup();
  }
}

void MainWindow::restoreData( QSettings& settings, DataFormat format )
{
  settings.beginGroup( "data" );
  m_expenses->restoreData( settings );
  settings.endGroup();
  if ( format == FullData )
  {
    settings.beginGroup( "layout" );
    restoreGeometry( settings.value( "geometry", QByteArray() ).toByteArray() );
    restoreState( settings.value( "state", QByteArray() ).toByteArray() );
    ui->expenseView->header()->restoreGeometry( settings.value( "expenseViewColumnsGeometry", QByteArray() ).toByteArray() );
    ui->expenseView->header()->restoreState( settings.value( "expenseViewColumnsState", QByteArray() ).toByteArray() );
    settings.endGroup();
  }
}

void MainWindow::exportIni()
{
  QString fileName = QFileDialog::getSaveFileName( this,
                                                   tr( "Export as INI file ... " ),
                                                   QString(),
                                                   tr( "INI File (*.ini)" ) );
  if ( ! fileName.isEmpty() )
  {
    QSettings settings( fileName, QSettings::IniFormat );
    saveData( settings, ExpenseDataOnly );
  }
}

void MainWindow::importIni()
{
  if ( QMessageBox::question( this,
                              tr( "Override Data?" ),
                              tr( "Importing a file will override all data which is currently loaded. Do you want to continue?" ),
                              QMessageBox::Yes, QMessageBox::No ) == QMessageBox::Yes )
  {
    QString fileName = QFileDialog::getOpenFileName( this,
                                                     tr( "Import from INI file ..." ),
                                                     QString(),
                                                     tr( "INI File (*.ini);;All (*.*)" ) );
    if ( !fileName.isEmpty() )
    {
      QSettings settings( fileName, QSettings::IniFormat );
      restoreData( settings, ExpenseDataOnly );
    }
  }
}

void MainWindow::addExpense()
{
  ExpenseEditDialog dialog( m_expenses, this );
  if ( dialog.exec() == QDialog::Accepted )
  {
    m_expenses->addExpense( dialog.expense() );
  }
}

void MainWindow::editExpense()
{
  if ( ui->expenseView->currentIndex().isValid() )
  {
    ExpenseEditDialog dialog( m_expenses, this );
    dialog.setExpense(
          m_expensesSortFilterProxyModel->expense(
            ui->expenseView->currentIndex() ) );
    if ( dialog.exec() == QDialog::Accepted )
    {
      m_expensesSortFilterProxyModel->replaceExpense(
            ui->expenseView->currentIndex(),
            dialog.expense() );
    }
  }
}

void MainWindow::removeExpense()
{
  if ( ui->expenseView->selectionModel()->selectedRows().count() > 0 )
  {
    if ( QMessageBox::question( this,
                                tr( "Remove Expenses" ),
                                tr( "Do you really want to remove the selected expenses? This cannot be undone." ),
                                QMessageBox::Yes, QMessageBox::No ) ==
         QMessageBox::Yes )
    {
      m_expensesSortFilterProxyModel->removeExpense(
            ui->expenseView->selectionModel()->selectedRows() );
    }
  }
}

void MainWindow::configureCategories()
{
  CategoryConfigurationDialog dialog( m_expenses, this );
  dialog.exec();
}

void MainWindow::itemDoubleClicked( const QModelIndex& )
{
  editExpense();
}
