#include "expense.h"

#include "category.h"

Expense::Expense( double amount,
                  const QString& description,
                  const QDate& date,
                  const QList< int >& categories ) :
  m_amount( amount ),
  m_description( description ),
  m_categories( categories ),
  m_date( date )
{
}

Expense::~Expense()
{
}

void Expense::saveData( QSettings &settings ) const
{
  settings.setValue( "amount", m_amount );
  settings.setValue( "description", m_description );
  settings.setValue( "date", m_date );
  settings.beginWriteArray( "categories", m_categories.size() );
  for ( int i = 0; i < m_categories.count(); ++i )
  {
    settings.setArrayIndex( i );
    settings.setValue( "category", m_categories[ i ] );
  }
  settings.endArray();
}

void Expense::restoreData( QSettings &settings )
{
  m_amount = settings.value( "amount", 0.0 ).toDouble();
  m_description = settings.value( "description", QString() ).toString();
  m_date = settings.value( "date", QDate() ).toDate();
  int numCategories = settings.beginReadArray( "categories" );
  m_categories.clear();
  for ( int i = 0; i < numCategories; ++i )
  {
    settings.setArrayIndex( i );
    addCategory( settings.value( "category", Category::InvalidCategory ).toInt() );
  }
  settings.endArray();
}
