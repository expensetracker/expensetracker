#include "expenseeditdialog.h"
#include "ui_expenseeditdialog.h"

#include "calculatordialog.h"
#include "categoryselectiondialog.h"

ExpenseEditDialog::ExpenseEditDialog( Expenses *expenses, QWidget *parent ) :
  QDialog(parent),
  ui(new Ui::ExpenseEditDialog),
  m_expenses( expenses )
{
  ui->setupUi(this);
  ui->date->setSelectedDate( QDate::currentDate() );

  connect( ui->editCategoriesButton, SIGNAL(clicked()),
           this, SLOT(editCategories()) );
  connect( ui->calculatorButton, SIGNAL(clicked()),
           this, SLOT(editAmount()) );
}

ExpenseEditDialog::~ExpenseEditDialog()
{
  delete ui;
}

Expense ExpenseEditDialog::expense() const
{
  return Expense( ui->amount->value(),
                  ui->description->text(),
                  ui->date->selectedDate(),
                  m_categories );
}

void ExpenseEditDialog::setExpense( const Expense &expense )
{
  ui->amount->setValue( expense.amount() );
  ui->description->setText( expense.description() );
  ui->date->setSelectedDate( expense.date() );
  m_categories = expense.categories();
  updateCategoryControl();
}

void ExpenseEditDialog::updateCategoryControl()
{
  QStringList categoryNames;
  foreach ( int catId, m_categories )
  {
    Category c = m_expenses->category( catId );
    if ( c.isValid() )
    {
      categoryNames << c.name();
    }
  }
  categoryNames.sort();
  ui->categories->setText( categoryNames.join( ", " ) );
}

void ExpenseEditDialog::editCategories()
{
  CategorySelectionDialog dialog( m_expenses, m_categories );
  if ( dialog.exec() )
  {
    m_categories = dialog.categories();
    updateCategoryControl();
  }
}

void ExpenseEditDialog::editAmount()
{
    CalculatorDialog dialog( this );
    dialog.setValue( ui->amount->value() );
    if ( dialog.exec() == QDialog::Accepted )
    {
        ui->amount->setValue( dialog.value() );
    }
}
