#include "categoryselectiondialog.h"
#include "ui_categoryselectiondialog.h"

CategorySelectionDialog::CategorySelectionDialog(Expenses *expenses,
                                                 const QList<int> &categories,
                                                 QWidget *parent) :
  QDialog(parent),
  ui(new Ui::CategorySelectionDialog)
{
  ui->setupUi(this);
  for ( int i = 0; i < expenses->categoryCount(); ++i )
  {
    Category c = expenses->categoryAt( i );
    QListWidgetItem* item = new QListWidgetItem( c.name() );
    item->setCheckState( categories.contains( c.id() ) ? Qt::Checked : Qt::Unchecked );
    item->setData( Qt::UserRole, c.id() );
    ui->categoryList->addItem( item );
  }
  ui->categoryList->sortItems();
}

CategorySelectionDialog::~CategorySelectionDialog()
{
  delete ui;
}

QList< int > CategorySelectionDialog::categories() const
{
  QList< int > result;
  for ( int i = 0; i < ui->categoryList->count(); ++i )
  {
    QListWidgetItem* item = ui->categoryList->item( i );
    if ( item->checkState() == Qt::Checked )
    {
      result << item->data( Qt::UserRole ).toInt();
    }
  }
  return result;
}
