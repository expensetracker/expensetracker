#include "expensesmodel.h"

#include <QStringList>

ExpensesModel::ExpensesModel( Expenses *expenses, QObject *parent ) :
  QAbstractItemModel(parent),
  m_expenses( expenses )
{
  connect( expenses, SIGNAL(expenseAdded(int)), this, SLOT(expenseAdded(int)) );
  connect( expenses, SIGNAL(expenseRemoved(int)), this, SLOT(expenseRemoved(int)) );
  connect( expenses, SIGNAL(expenseChanged(int)), this, SLOT(expenseChanged(int)) );
  connect( expenses, SIGNAL(expensesReset()), this, SLOT(expensesReset()) );
  connect( expenses, SIGNAL(categoriesChanged()), this, SLOT(categoriesChanged()) );
}

ExpensesModel::~ExpensesModel()
{
}

const Expense& ExpensesModel::expense( const QModelIndex &index ) const
{
  return m_expenses->expense( index.row() );
}

void ExpensesModel::removeExpense( const QModelIndexList &indices )
{
  QList< int > indicesToRemove;
  foreach ( QModelIndex idx, indices )
  {
    indicesToRemove.append( idx.row() );
  }
  qSort( indicesToRemove );
  for ( int i = indicesToRemove.count(); i != 0; --i )
  {
    m_expenses->removeExpense( indicesToRemove.at( i - 1 ) );
  }
}

void ExpensesModel::replaceExpense( const QModelIndex &index, Expense newExpense )
{
  m_expenses->replaceExpense( index.row(), newExpense );
}

int ExpensesModel::rowCount( const QModelIndex &parent ) const
{
  if ( parent.isValid() )
  {
    return 0;
  }
  return m_expenses->expenseCount();
}

int ExpensesModel::columnCount( const QModelIndex &parent ) const
{
  if ( parent.isValid() )
  {
    return 0;
  }
  return ColumnCount;
}

QModelIndex ExpensesModel::index( int row,
                                  int column,
                                  const QModelIndex &parent ) const
{
  if ( parent.isValid() )
  {
    return QModelIndex();
  }
  return createIndex( row, column );
}

QModelIndex ExpensesModel::parent( const QModelIndex& ) const
{
  return QModelIndex();
}

QVariant ExpensesModel::headerData( int section,
                                    Qt::Orientation orientation,
                                    int role ) const
{
  if ( orientation == Qt::Vertical )
  {
    return QVariant();
  }
  switch ( role )
  {
  case Qt::DisplayRole:
  {
    switch ( section )
    {
    case AmountColumn: return tr( "Amount" );
    case DescriptionColumn: return tr( "Description" );
    case DateColumn: return tr( "Date" );
    case CategoriesColumn: return tr( "Categories" );
    default: return QVariant();
    }
  }

  default: return QVariant();
  }
}

QVariant ExpensesModel::data( const QModelIndex &index, int role ) const
{
  if ( ! index.isValid() || index.row() < 0 || index.row() >= rowCount() ||
       index.column() < 0 || index.column() >= columnCount() )
  {
    return QVariant();
  }

  switch ( role )
  {
  case Qt::DisplayRole:
  {
    switch ( index.column() )
    {
    case AmountColumn: return QString( "%1" ).arg( m_expenses->expense( index.row() ).amount(), 0, 'f', 2, QLatin1Char( '0' ) );
    case DescriptionColumn: return m_expenses->expense( index.row() ).description();
    case DateColumn: return m_expenses->expense( index.row() ).date().toString();
    case CategoriesColumn:
    {
      QStringList categories;
      foreach ( int categoryId, m_expenses->expense( index.row() ).categories() )
      {
        Category category = m_expenses->category( categoryId );
        if ( category.isValid() )
        {
          categories << category.name();
        }
      }
      return categories.join( ", " );
    }
    default: return QVariant();
    }
  }

  case Qt::TextAlignmentRole:
  {
    switch ( index.column() )
    {
    case AmountColumn: return Qt::AlignRight;
    default: return QVariant();
    }
  }

  default: QVariant();
  }
  return QVariant();
}

void ExpensesModel::expenseAdded( int index )
{
  emit beginInsertRows( QModelIndex(), index, index );
  emit endInsertRows();
}

void ExpensesModel::expenseRemoved( int index )
{
  emit beginRemoveRows( QModelIndex(), index, index );
  emit endRemoveRows();
}

void ExpensesModel::expenseChanged( int index )
{
  emit dataChanged( this->index( index, 0 ),
                    this->index( index, columnCount() - 1 ) );
}

void ExpensesModel::expensesReset()
{
  emit beginResetModel();
  emit endResetModel();
}

void ExpensesModel::categoriesChanged()
{
  emit dataChanged( index( 0, 0 ),
                    index( rowCount(), columnCount() ) );
}


