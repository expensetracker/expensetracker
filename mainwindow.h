#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "expenses.h"
#include "expensesmodel.h"
#include "expensessortfilterproxymodel.h"

#include <QMainWindow>

class GraphView;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

  typedef enum
  {
    FullData,
    ExpenseDataOnly
  } DataFormat;

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private:
  Ui::MainWindow*                 ui;

  Expenses*                       m_expenses;
  ExpensesModel*                  m_expensesModel;
  ExpensesSortFilterProxyModel*   m_expensesSortFilterProxyModel;

  GraphView*                      m_graphView;


  void saveData( QSettings& settings, DataFormat format = FullData ) const;
  void restoreData( QSettings& settings, DataFormat format = FullData );

private slots:

  void exportIni();
  void importIni();
  void addExpense();
  void editExpense();
  void removeExpense();
  void configureCategories();

  void itemDoubleClicked( const QModelIndex& index );

};

#endif // MAINWINDOW_H
