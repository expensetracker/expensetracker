#include "categoryconfigurationdialog.h"
#include "ui_categoryconfigurationdialog.h"

#include <QInputDialog>
#include <QPushButton>
#include <QMessageBox>

CategoryConfigurationDialog::CategoryConfigurationDialog(Expenses* expenses, QWidget *parent) :
  QDialog(parent),
  ui(new Ui::CategoryConfigurationDialog),
  m_expenses( expenses )
{
  ui->setupUi(this);

  QPushButton* addCategoryButton = new QPushButton( tr( "Add" ), this );
  QPushButton* removeCategoryButton = new QPushButton( tr( "Delete" ), this );
  QPushButton* renameCategoryButton = new QPushButton( tr( "Rename" ), this );

  ui->buttonBox->addButton( addCategoryButton, QDialogButtonBox::ActionRole );
  ui->buttonBox->addButton( removeCategoryButton, QDialogButtonBox::ActionRole );
  ui->buttonBox->addButton( renameCategoryButton, QDialogButtonBox::ActionRole );

  connect( addCategoryButton, SIGNAL(clicked()), this, SLOT(addCategory()) );
  connect( renameCategoryButton, SIGNAL(clicked()), this, SLOT(renameCategory()) );
  connect( removeCategoryButton, SIGNAL(clicked()), this, SLOT(deleteCategory()) );

  for ( int i = 0; i < m_expenses->categoryCount(); ++i )
  {
    Category c = m_expenses->categoryAt( i );
    QListWidgetItem* item = new QListWidgetItem( c.name() );
    item->setData( Qt::UserRole, c.id() );
    ui->categoryList->addItem( item );
  }
  ui->categoryList->sortItems();
}

CategoryConfigurationDialog::~CategoryConfigurationDialog()
{
  delete ui;
}

void CategoryConfigurationDialog::addCategory()
{
  QString text;
  bool ok;
  text = QInputDialog::getText( this,
                                tr( "Category Name" ),
                                tr( "Please specify the name for the new category" ),
                                QLineEdit::Normal,
                                tr( "New Category" ),
                                &ok );
  if ( ok )
  {
    int i = 0;
    while ( m_expenses->category( i ).isValid() )
    {
      i++;
    }
    Category c( i, text );
    m_expenses->addCategory( c );
    QListWidgetItem* item = new QListWidgetItem( text );
    item->setData( Qt::UserRole, c.id() );
    ui->categoryList->addItem( item );
    ui->categoryList->sortItems();
  }
}

void CategoryConfigurationDialog::renameCategory()
{
  if ( ui->categoryList->currentItem() )
  {
    Category category = m_expenses->category(
          ui->categoryList->currentItem()->data( Qt::UserRole ).toInt() );
    if ( category.isValid() )
    {
      QString text;
      bool ok;
      text = QInputDialog::getText( this,
                                    tr( "Rename Category" ),
                                    tr( "Please specify a new name for the category" ),
                                    QLineEdit::Normal,
                                    category.name(),
                                    &ok );
      if ( ok )
      {
        category.setName( text );
        m_expenses->replaceCategory( category );
        ui->categoryList->currentItem()->setText( text );
        ui->categoryList->sortItems();
      }
    }
  }
}

void CategoryConfigurationDialog::deleteCategory()
{
  if ( ui->categoryList->currentItem() )
  {
    Category c = m_expenses->category(
          ui->categoryList->currentItem()->data( Qt::UserRole ).toInt() );
    if ( c.isValid() )
    {
      if ( QMessageBox::question( this,
                                  tr( "Delete Category" ),
                                  tr( "Do you really want to delete this category?" ),
                                  QMessageBox::Yes,
                                  QMessageBox::No ) == QMessageBox::Yes )
      {
        m_expenses->removeCategory( c );
        QListWidgetItem* item = ui->categoryList->takeItem(
              ui->categoryList->currentRow() );
        delete item;
      }
    }
  }
}
