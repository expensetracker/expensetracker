#include "graphview.h"

#include <QFile>
#include <QGridLayout>
#include <QTextStream>

GraphView::GraphView(Expenses* expenses, QWidget *parent) :
    QWidget(parent),
    m_expenses( expenses ),
    m_view( new QWebView( this ) )
{
    setLayout( new QGridLayout( this ) );
    layout()->addWidget( m_view );

    QFile f( ":/graphviewtemplate.html" );
    if ( f.open( QIODevice::ReadOnly ) ) {
        m_pageTemplate = f.readAll();
        f.close();
    }

    connect( m_expenses, SIGNAL(expenseAdded(int)), this, SLOT(redraw()) );
    connect( m_expenses, SIGNAL(expenseChanged(int)), this, SLOT(redraw()) );
    connect( m_expenses, SIGNAL(expenseRemoved(int)), this, SLOT(redraw()) );
    connect( m_expenses, SIGNAL(expensesReset()), this, SLOT(redraw()) );
    connect( m_expenses, SIGNAL(categoriesChanged()), this, SLOT(redraw()) );

}

void GraphView::redraw()
{
    updateGraph();
}

void GraphView::updateGraph() {
    QString csv = accumulatedExpensesToCsvString(
                accumulateExpenses() );
    QString html = m_pageTemplate.replace( "%CSV%", "\"" + csv + "\"" );
    m_view->setHtml( html );
}

QHash< quint64, GraphView::ExpensesPerYearAndMonth >
GraphView::accumulateExpenses() const
{
    QHash< quint64, ExpensesPerYearAndMonth > accumulatedStatistics;
    for ( int i = 0; i < m_expenses->expenseCount(); ++i ) {
        const Expense& e = m_expenses->expense( i );
        quint64 key = ( (quint64) e.date().year() << 32 ) | e.date().month();
        ExpensesPerYearAndMonth acc = accumulatedStatistics.value(
                    key, ExpensesPerYearAndMonth() );
        acc.year = e.date().year();
        acc.month = e.date().month();
        acc.totalExpense += e.amount();
        foreach ( int category, e.categories() ) {
            acc.totalExpensePerCategory.insert(
                        category,
                        acc.totalExpensePerCategory.value( category, 0 ) +
                        e.amount() );
        }
        accumulatedStatistics.insert( key, acc );
    }
    return accumulatedStatistics;
}

QString GraphView::accumulatedExpensesToCsvString(
        QHash<quint64, ExpensesPerYearAndMonth> accumulatedExpenses) const
{
    QString result;
    QTextStream stream( &result, QIODevice::WriteOnly );
    stream << "Year-Month,Total";
    for ( int i = 0; i < m_expenses->categoryCount(); ++i ) {
        const Category& category = m_expenses->categoryAt( i );
        QString categoryName = category.name();
        categoryName = categoryName.split( "\"" ).join( "" );
        stream << "," << categoryName;
    }
    stream << "\\n";
    QList< quint64 > keys = accumulatedExpenses.keys();
    qSort( keys );
    foreach ( quint64 key, keys ) {
        ExpensesPerYearAndMonth& expenses = accumulatedExpenses[ key ];
        int month = key & 0xFFFFFFFF;
        int year = ( key >> 32 ) & 0xFFFFFFFF;
        stream << year << "-" << month << "-01," << expenses.totalExpense;
        for ( int i = 0; i < m_expenses->categoryCount(); ++i ) {
            int categoryId = m_expenses->categoryAt( i ).id();
            stream << "," << expenses.totalExpensePerCategory.value(
                          categoryId, 0.0 );
        }
        stream << "\\n";
    }
    return result;
}
